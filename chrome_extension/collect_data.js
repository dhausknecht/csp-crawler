/*****************************************************************************
 * Variable definitions                                                      *
 *****************************************************************************/
var DEBUG = false;
var latestHeaders = null;

onStartUp(function() {
  console.log("Browser cache cleared.");
});

/*****************************************************************************
 * Function definitions                                                      *
 *****************************************************************************/
chrome.webRequest.onBeforeRequest.addListener(function(details) {
    if (details && (details.frameId === 0) && (details.type === "main_frame")
        && details.url.indexOf("http") === 0 && details.url.indexOf('?') === -1) {
      console.log("Redirecting "+details.url+" (frameId: "+details.frameId+")");
      latestHeaders = null;
      details.url = details.url+"?"+(new Date().getTime());
      return {redirectUrl: details.url};
    }
  },
  {"urls": ["<all_urls>"]},
  ["blocking"]
);

chrome.webRequest.onHeadersReceived.addListener(function(details) {
    if ((details && (details.frameId === 0)) && (details.type === "main_frame")) {
      if (details && details.responseHeaders) {
        //console.log("Response for "+details.url+" (frameId: "+details.frameId+")");
        details.responseHeaders.forEach(function(header) {
          //console.log(header.name);
          if (header.name.match(new RegExp("content-security-policy|^x-.*-csp(-report-only)?","i"))) {
            latestHeaders = {
              url: details.url,
              header: { name: header.name, value: header.value }
            };
            console.log("CSP found! url: "+details.url);
          }
        });
      }
    }
  },
  {"urls": ["<all_urls>"]},
  ["responseHeaders"]
);

function getHeaderInfo() {
  return latestHeaders;
}

function onStartUp(callback) {
  if (!DEBUG) { console.log = function() {}; }

  // empty the full cache on start up
  chrome.browsingData.remove({
    "since": 0
    /*,"originTypes": { "protectedWeb": true }*/
  }, {
    "appcache": true,
    "cache": true,
    "cookies": true,
    "downloads": true,
    "fileSystems": true,
    "formData": true,
    "history": true,
    "indexedDB": true,
    "localStorage": true,
    "serverBoundCertificates": true,
    "pluginData": true,
    "passwords": true,
    "webSQL": true
  }, callback);
}
