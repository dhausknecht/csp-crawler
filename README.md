# Web crawler #

The code provides the functionality of a web crawler based on Nodejs and Phantomjs.
It's main purpose for now is to collect CSP headers (in HTTP and as HTML meta tags).

### What it does ###

1) Based on certain parameters, the number of needed parallel processes to fulfil the requirements are computed. The parameters are:

  * the number of domains
  * the total time you give it to do the job
  * the time it should spend on each web page


2) It starts the processes and logs various outputs:

  * the gathered data (for example the CSP headers)
  * all console output from the webpages themselves
  * some additional output what the crawler is doing at the moment

### How to run it ###

1) Change to the src folder

2) Run for example the nodejs script "concurrent_runs_phantomjs.js" as follows to visit the first 500 domains of the Alexa Top 1Mio with 500 minutes total time and 30 seconds per page load:

```
#!bash

nodejs concurrent_runs_phantomjs.js -d 500 -tt 500 -spl 30
```

To only get the number of processes without actually starting them, simply add the "--buckets" option to the above line.

To run a certain specific process manually, you can do as follows (the example shows the command for the second process started by the command above):

```
#!bash

nodejs phantomjs_single_domain.js -csv ../top-1m.csv --start 251 --end 500 -plt 30
```

Note, that this will print the log output to the console (not the gathered information though).
To conveniently redirect it into a file, add for example

```
one_of_the_above_commands > ../logs/logfile_251_500.log 2>&1 &
```

to the end of the command.


### Command line options ###

For "concurrent_runs_phantomjs.js":

* **-d / --domains**: the number of top domains from the Alexa Top 1Mio list
* **-t / --totaltime**: the overall time used for the entire experiment
* **-spl / --secperload**: the number of seconds per page visit
* **-h / --headers**: on "true" the HTTP headers and their respective count is recorded
* **--buckets**: only outputs the number of processes without actually starting them

For "phantomjs_single_domain.js":

* **--example**: adds "example.com" to the domains list
* **-d / --domain <some url>**: adds the specified domain to the domain list. Note that you should omit the protocol and the "www" subdomain because they are added by the script automatically while crawling.
* **-csv <path to file>**: specifies the location of the file with the domain list. The repo comes with a CSV file of the Alexa Top 1Mio which you might want to use.
* **-s / --start <number>**: sets the domain to start crawling with.
* **-e / --end <number>**: sets the domain to end crawling with.
* **-plt <number>**: sets the page loading time, that is the seconds it should spend on each page.
* **-h / --headers**: on "true" the HTTP headers and their respective count is recorded

### Note ###

* You very likely need to adjust some hardcoded file paths in the scripts (quick and dirty hacking, sorry for that...)
* When running it on a remote server, you might want to use a tool like **tmux** to not kill the processes when you log out from the ssh session.
* Sorry for the weird names of the files. They are for project evolution reasons. I didn't expect Phantomjs to randomly crash, so I designed things differently in the beginning and gave it the cool names already.
