// module imports
var page = require('webpage').create();
var system = require('system');
var fs = require('fs');

var EXIT_OKAY = 0;
var EXIT_ERROR = 1;
var EXIT_FAILED_PAGE_LOAD = 2;

/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/

var CSP_REGEX = new RegExp("content-security-policy|^x-.*-csp(-report-only)?","i");
var LOG_FILE_NAME = null;
var LOG_HTTP_HEADERS = false;
var PAGE_LOADING_TIME = 3000;
var PAGE_LOAD_TIMEOUT = 3100;
var domainName = null;
var domainIndex = -1;
var urlsToVisit = null;
var currentURL = 0;
var program_exit_code = EXIT_OKAY;

var loadingStages = {
  currentStage: 0,
  stages: [
    { name: "response", startTime: new Date().getTime() },
    { name: "onLoadStarted", startTime: null },
    { name: "onLoadFinished", startTime: null }
  ],
  nextStage: function() {
    if (this.currentStage < this.stages.length-1) {
      this.currentStage += 1;
      this.stages[this.currentStage].startTime = new Date().getTime();
    }
  },
  getCurrentStage: function() {
    return this.stages[this.currentStage].name;
  },
  getStageAt: function(time_in_ms) {
    for (var i = 0; i < this.stages.length; i++) {
      var s_t = (this.stages[i].startTime) ? this.stages[i].startTime : new Date().getTime();
      if (time_in_ms < s_t) {
        return (0 < i) ? this.stages[i-1].name : this.stages[0].name;
      }
    }
    return this.stages[this.stages.length-1].name;
  }
};

main(system.args);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameter(args);
  initPage();
  visitURLs();
}

function setDomainName(domain) {
  function createUrlDataset(prefix) {
    var dataset = {
      url: prefix+domain,
      policy: [],
      warnings: [],
      metaTags: []
    };
    if (LOG_HTTP_HEADERS) {
      dataset.responses = { num_resp: 0, httpHeaders: {} };
    }
    return dataset;
  }
  urlsToVisit = [
      createUrlDataset("http://"),
      createUrlDataset("http://www."),
      createUrlDataset("https://"),
      createUrlDataset("https://www.")
    ];
}


function init_parameter(args) {
  // args[0] = script
  for (var i = 1; i < args.length; i++) {
    if (args[i] == "-example") {
      domainName = "example.com";
    } else
    if ((args[i] == "-d" || args[i] == "--domain") && i+1 < args.length) {
      domainName = args[i+1];
      i++;
    } else
    if ((args[i] == "-i" || args[i] == "--index") && i+1 < args.length) {
      domainIndex = args[i+1];
      i++;
    } else
    if ((args[i] == "-l" || args[i] == "--logfile") && i+1 < args.length) {
      LOG_FILE_NAME = args[i+1];
      i++;
    } else
    if (args[i] == "-plt" && i+1 < args.length) {
      PAGE_LOADING_TIME = parseInt(args[i+1])*1000;
      PAGE_LOAD_TIMEOUT = PAGE_LOADING_TIME+1000;
      i++;
    } else
    if ((args[i] == "-h" || args[i] == "--headers") && i+1 < args.length) {
      LOG_HTTP_HEADERS = (args[i+1] === "true");
      i++;
    } else {
      log("E", help_str());
      phantom.exit(EXIT_ERROR);
    }
  }
  setDomainName(domainName);
}


function initPage() {
  page.settings.userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"
  page.onError = function(msg, trace) {
    log("E", "ERROR: " + msg);
    //log("D", "TRACE: "+JSON.stringify(trace));
    program_exit_code = EXIT_FAILED_PAGE_LOAD;
  };
  page.viewportSize = { width: 1920, height: 1080 };

  page.onConsoleMessage = function(msg, lineNum, sourceId) {
    log("", "CONSOLE: "+msg);
  };

  page.onInitialized = function() {
    var success = page.evaluate(function() {
      var config = { childList: true, subtree: true };
      var onObservedFunction = function(mutations, that) {
        if (typeof window.callPhantom === 'function') {
          var nodes = [];
          mutations.forEach(function(mutation) {
            for (var i = 0; i < mutation.addedNodes.length; i++) {
              var item = mutation.addedNodes.item(i);
              if ((item && item.tagName && item.baseURI && item.attributes)
                  && (item.tagName.toUpperCase() === "META")) {
                nodes.push({
                  baseURI: item.baseURI,
                  attributes: item.attributes,
                  stageTime: new Date().getTime()
                });
              } else
              if (item && item.tagName && item.tagName.toUpperCase() === "IFRAME") {
                //console.log("D", "iframe src: "+item.src);
                var mo = new MutationObserver(onObservedFunction);
                mo.observe(item.contentWindow.document, config);
              }
            };
          }); // forEach end
          window.callPhantom(nodes);
        }
        that.takeRecords(); // flush it
      } // mutation observer function end
      var mo = new MutationObserver(onObservedFunction);
      mo.observe(document, config);
      return true;
    }); // injection script end

    if (success) {
      log("I", 'DOM listener injected into page sucessfully.');
    } else {
      log("E", 'Failed to inject DOM listener into page.');
    }
  };

  page.onLoadStarted = function() {
    loadingStages.nextStage();
  };
  page.onLoadFinished = function() {
    loadingStages.nextStage();
  };
}


function visitURLs() {
  setTimeout(function() {
    if (currentURL < urlsToVisit.length) {
      page.clearCookies();
      page.clearMemoryCache();
      visitWebpage(currentURL);
      currentURL += 1;
    } else {
      shutdown();
    }
  },0);
}


function visitWebpage(urlIndex) {
  /*page.onResourceRequested = function(data,request) {
    console.log("onResourceRequested: id: "+data.id+", url: "+data.url);
  };//*/
  page.onCallback = function(data) {
    for (var i = 0; i < data.length; i++) {
      if (data[i] && data[i].baseURI && data[i].attributes && data[i].stageTime) {
        var header_name = null;
        var value = null;
        var stage = loadingStages.getStageAt(data[i].stageTime);
        for (var a = 0; a < data[i].attributes.length; a++) {
          var attr = data[i].attributes[a];
          if (attr.name.match(new RegExp("http-equiv","i"))) {
            header_name = attr.value;
          } else
          if (attr.name.match(new RegExp("content","i"))) {
            value = attr.value;
          }
        } // for attributes.length
        if (header_name && value && stage) {
          var record = {
            url: data[i].baseURI,
            meta: { name: header_name, value: value },
            stage: stage
          };
          if (header_name.match(CSP_REGEX)) {
            urlsToVisit[urlIndex].policy.push(record);
          } else {
            urlsToVisit[urlIndex].meta.push(record);
          }
          log("I", "META http-equiv='"+header_name+"' content='"+value+"'");
        }
      }
    }
  };

  page.onResourceError = function(resourceError) {
    log("I", "Page load failed: "+JSON.stringify(resourceError));
    urlsToVisit[urlIndex].warnings.push(reason);
    clearTimeout(timeoutFunction);
    timeoutFunction();
  };

  page.onResourceReceived = function(response) {
    //console.log("onResourceReceived: id: "+response.id+", url: "+response.url+", stage: "+response.stage);
    if (response && response.stage && response.headers) {
      if(response.stage == 'end') return;
      if (LOG_HTTP_HEADERS) {
        urlsToVisit[urlIndex].responses.num_resp += 1;
      }
      response.headers.forEach(function(header) {
        if (header.name.match(CSP_REGEX)) {
          var responseURL = (response.redirectURL) ? response.redirectURL : response.url;
          urlsToVisit[urlIndex].policy.push({
            url: responseURL,
            http: { name: header.name, value: header.value }
          });
        }
        if (LOG_HTTP_HEADERS) {
          var lcHeader_str = header.name.toLowerCase();
          if (! urlsToVisit[urlIndex].responses.httpHeaders[lcHeader_str]) {
            urlsToVisit[urlIndex].responses.httpHeaders[lcHeader_str] = 0;
          }
          urlsToVisit[urlIndex].responses.httpHeaders[lcHeader_str] += 1;
        }
      });
    }
  };

  log("I", "Trying to load '"+urlsToVisit[urlIndex].url+"'...");
  page.open(
    urlsToVisit[urlIndex].url,
    function(status) {
      if (status === "success") {
        onLoadSuccess();
      }
      // else case this is handled by onResourceError function
    }
  );
  // wait on the current page to allow dynamic subresources to be loaded
  // do it here to guarantee a max page loading time (incl. page loading
  // itself)
  window.setTimeout(timeoutFunction, PAGE_LOAD_TIMEOUT);
}


function onLoadSuccess() {
  log("I", "Page loaded. Waiting for subresources...");
};


function timeoutFunction() {
  visitURLs();
}


function appendToFile(data, file_path, callback) {
  log("I", "Writing logs to file '"+file_path+"'...");
  try {
    //var outputStr = JSON.stringify(data, null, 2);
    //outputStr = outputStr.substring(1, outputStr.length-1)+",";
    var stream = fs.open(file_path, 'a');
    stream.write(JSON.stringify(data, null, 2));
    stream.close();
  } catch (e) {
    log("E", "Failed to write to file: "+e.message);
  } finally {
    setTimeout(callback, 0);
  }
}


function shutdown() {
  var domainData = {
    domain: domainName,
    visitedURLs: urlsToVisit
  };
  appendToFile(domainData, LOG_FILE_NAME, function() {
    log("I", 'Browser is shutting down now.');
    page.close();
    phantom.exit(program_exit_code);
  });
}


function log(mode, msg) {
  system.stdout.writeLine("[client.js] - " + msg);
}


function help_str() {
  return "TODO: write useful help text";
}
