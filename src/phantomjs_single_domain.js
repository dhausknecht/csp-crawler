'use strict'

var fs = require('fs');
var child_process = require('child_process');

var EXIT_OKAY = 0;
var EXIT_ERROR = 1;
var EXIT_FAILED_PAGE_LOAD = 2;

/******************************************************************************
 * Constants                                                                  *
 ******************************************************************************/

var CSV_FILE = null;
var START_DOMAIN = 0;
var END_DOMAIN = 0;
var PAGE_LOADING_TIME = 0;
var LOG_FILE_NAME = null;
var JSON_FILE_NAME = null;
var LOG_HTTP_HEADERS = false;

var USER = process.env.USER;
var PHANTOMJS_OPTIONS = "--ssl-protocol=any --ignore-ssl-errors=true"
var PHANTOMJS = "/home/"+USER+"/phantomjs_code/phantomjs/bin/phantomjs "+PHANTOMJS_OPTIONS;

/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/

var domains = new Array();
var error_domains = new Array();
var failed_page_load_domains = new Array();

main(process.argv);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameters(args);
  readDomainsFromFile(CSV_FILE, () => {
    // open the array in the JSON file before we start crawling
    appendToFile("[\n", JSON_FILE_NAME, function() {
      //log("D", JSON.stringify(domains), true);
      visitNextDomain(true);
    }, false);
  });
}

function visitNextDomain(isFirst) {
  setTimeout(() => {
    if (0 < domains.length) {
      var domain = domains.shift();
      if (0 < domains.length && !isFirst) {
        appendToFile(",\n", JSON_FILE_NAME, function() {
          domain.visitDomain();
        });
      } else {
        domain.visitDomain();
      }
    } else {
      shutdown();
    }
  }, 0);
}


function init_parameters(args) {
  // "nodejs" and the script file are 0 and 1, respectively
  for (var i = 2; i < args.length; i++) {
    if (args[i] == "--example") {
      domains.push(new Domain(domains.length+1, "example.com"));
    } else
    if (args[i] == "-csv" && i+1 < args.length) {
      CSV_FILE = ""+args[i+1];
      i++;
    } else
    if ((args[i] == "-d" || args[i] == "--domain") && i+1 < args.length) {
      domains.push(new Domain(domains.length+1, args[i+1]));
      i++;
    } else
    if ((args[i] == "-s" || args[i] == "--start") && i+1 < args.length) {
      START_DOMAIN = parseInt(args[i+1]);
      i++;
    } else
    if ((args[i] == "-e" || args[i] == "--end") && i+1 < args.length) {
      END_DOMAIN = parseInt(args[i+1]);
      i++;
    } else
    if (args[i] == "-plt" && i+1 < args.length) {
      PAGE_LOADING_TIME = parseInt(args[i+1]);//*1000;
      i++;
    } else
    if ((args[i] == "-h" || args[i] == "--headers") && i+1 < args.length) {
      LOG_HTTP_HEADERS = (args[i+1] === "true");
      i++;
    } else {
      log("E", help_str(), true);
      process.exit(1);
    }
  }
  var file_name = "../logs/"
                    +(new Date()).toISOString().slice(0,10).replace(/-/g,"")
                    +"_"+START_DOMAIN+"_"+END_DOMAIN;
  LOG_FILE_NAME = file_name+".log";
  JSON_FILE_NAME = file_name+".json";
}


function readDomainsFromFile(csv_file, callback) {
  // helper function
  function isString(x) {
    return (typeof x === 'string' || x instanceof String);
  };

  log("I", "calling readDomainsFromFile", true);
  if (isString(csv_file) && domains && domains.length == 0) {
    log("I", `Reading domains from CSV file ${csv_file}...`, true);
    try {
      var lineReader = require('readline').createInterface({
        input: fs.createReadStream(csv_file)
      });
      lineReader.on('line', (line) => {
        var data = line.split(",");
        if (START_DOMAIN <= data[0] && data[0] <= END_DOMAIN) {
          //log("D", `Adding ${data}...`, true);
          domains.push(new Domain(data[0],data[1]));
          // stop reading when we read the last domain for our instance
          if (END_DOMAIN <= data[0]) { lineReader.close(); }
        }
      });
      lineReader.on('close', () => {
        log("I", "Initialisation complete", true);
        setTimeout(callback,0);
      });
    } catch (e) {
      log("E", `Failed to read from CSV: ${e.message}`, true);
      log("E", +Object.keys(e), true);
      process.exit(1);
    }
  } else {
    log("I", "Manual URL list detected. Ignoring CSV file.", true);
    setTimeout(callback,0);
  }
}


function Domain(index, domainName) {
  this.index = index;
  this.domainName = domainName;

  this.visitDomain = function() {
    var bash_cmd = PHANTOMJS+" client_phantomjs_single_domain.js "
                    +" -d "+this.domainName
                    +" -i "+this.index
                    +" -plt "+PAGE_LOADING_TIME
                    +" -l "+JSON_FILE_NAME
                    +" -h "+LOG_HTTP_HEADERS;

    var options = {
      cwd: process.cwd(),
      shell: "bash",
      uid: 1000,
      gid: 1000
    };

    log("I", `Visiting domain: ${this.domainName} (index: ${this.index})`, true);
    var child = child_process.exec(bash_cmd, options);
    child.stdout.on('data', (data) => { log("I", data, false, ""); });
    child.stderr.on('data', (data) => { log("E", data, false, ""); });
    child.on("close", (exit_code) => {
      if (exit_code === EXIT_FAILED_PAGE_LOAD) {
        failed_page_load_domains.push(this);
      } else
      if (exit_code !== EXIT_OKAY) {
        error_domains.push(this);
      }
      visitNextDomain(false);
    });
  }
}

function shutdown() {
  setTimeout(() => {
    // print list of domains for which loading failed somehow
    var outputStr = "List of domains with failed page load:\n";
    failed_page_load_domains.forEach((failed) => {
      outputStr += failed.index+","+failed.domainName+"\n";
    });
    log("I", outputStr, true);
    outputStr = "List of completely failed domains:\n";
    error_domains.forEach((error) => {
      outputStr += error.index+","+error.domainName+"\n";
    });
    log("I", outputStr, true);

    // close the array in the report file
    appendToFile("\n]", JSON_FILE_NAME, () => {
      log("I", "\nShutting down. Bye :-)", true);
      process.exit(0);
    });
  },0);
}


function appendToFile(data, file_path, callback, append=true) {
  log("I", `Writing logs to file '${file_path}'...`, true);
  var writeFunction = (append) ? fs.appendFile : fs.writeFile ;
  writeFunction(file_path, data, (err) => {
    if (err) {
      log("E", `Failed to write to file: ${e.message}`, true);
    } else {
      setTimeout(callback, 0);
    }
  });
}


function help_str() {
  return "TODO: useful help string. Sorry for that...";
}


function log(mode, msg, newLine=false, descriptor=" [phantomjs_single_domain.js] -") {
  if (newLine) {
    console.log(`${mode}${descriptor} ${msg}`);
  } else {
    process.stdout.write(`${mode}${descriptor} ${msg}`);
  }
}
