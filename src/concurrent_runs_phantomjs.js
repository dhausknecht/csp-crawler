'use strict'

// module imports
const exec = require('child_process').execFile;

// program configurarions variables
var PAUSE = 20*1000;
var NUM_DOMAINS = 1;
var TOTAL_TIME_IN_MIN = 60;
var SEC_PER_PAGE_LOAD = 30;
var BUCKETS_TEST = false;
var START = 1;
var END = 1;
var LOG_HTTP_HEADERS = false;

var USER = process.env.USER;
var PHANTOMJS_OPTIONS = "--ssl-protocol=any --ignore-ssl-errors=true"
//var PHANTOMJS = "/home/"+USER+"/experiments/phantomjs/bin/phantomjs "+PHANTOMJS_OPTIONS;
var PHANTOMJS = "/home/"+USER+"/phantomjs_code/phantomjs/bin/phantomjs "+PHANTOMJS_OPTIONS;


/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/

main(process.argv);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameter(args);

  // times 4 because of http://, https://, http://www. and https://www.
  var num_buckets = Math.ceil( (NUM_DOMAINS*4) / (TOTAL_TIME_IN_MIN * (60/SEC_PER_PAGE_LOAD)) );
  var bucket_size = Math.ceil( NUM_DOMAINS / num_buckets );

  log("I", "Creating "+num_buckets+" buckets of size "+bucket_size);

  if (BUCKETS_TEST) { process.exit(); }

  var start = START;
  var end = END;
  var threshold = START + NUM_DOMAINS - 1; // -1 because we start with START=1

  spawnBrowser();

  function spawnBrowser() {
    end = start + bucket_size - 1;
    if (threshold < end) {
      end = threshold;
    }

    log("I", "Start: "+start+", end: "+end);
    startCrawling(start, end, SEC_PER_PAGE_LOAD, LOG_HTTP_HEADERS);

    start = end + 1;
    if (start <= threshold) { setTimeout(spawnBrowser, PAUSE); }
  }

}


function startCrawling(start, end, page_loading_in_sec, log_headers) {
  var log_file_name = (new Date()).toISOString().slice(0,10).replace(/-/g,"")
                          +"_"+start+"_"+end+".log";
  var bash_cmd = "nodejs phantomjs_single_domain.js -csv ../top-1m.csv"
                  +" --start "+start+" --end "+end+" -plt "+page_loading_in_sec
                  +" --headers "+log_headers;
  bash_cmd += " > ../logs/"+log_file_name+" 2>&1 &";

  var options = {
    cwd: process.cwd(),
    shell: "bash",
    uid: 1000,
    gid: 1000
  };

  log("I", "Starting process: "+bash_cmd);
  exec(bash_cmd, options, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    if (stdout) {
      console.log(`stdout: ${stdout}`);
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
    }
  });
}


function init_parameter(args) {
  for (var i = 0; i < args.length; i++) {
    if (2 <= i) { // "nodejs" and the script file are 0 and 1, respectively
      if (args[i] == "--buckets") {
        BUCKETS_TEST = true;
      } else
      if ((args[i] == "--start" || args[i] == "-s") && i+1 < args.length) {
        START = END = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "--domains" || args[i] == "-d") && i+1 < args.length) {
        NUM_DOMAINS = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "--totaltime" || args[i] == "-tt") && i+1 < args.length) {
        TOTAL_TIME_IN_MIN = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "--secperload" || args[i] == "-spl") && i+1 < args.length) {
        SEC_PER_PAGE_LOAD = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "-h" || args[i] == "--headers") && i+1 < args.length) {
        LOG_HTTP_HEADERS = (args[i+1] === "true");
        i++;
      } else {
        log("E", help_str());
        process.exit();
      }
    }
  }
}


function help_str() {
  return "Usage: TODO";
}


function log(mode, msg, update=false) {
  if (update) {
    process.stdout.write(mode + " [client.js] - " + msg+'\r');
  } else {
    console.log(mode + " [client.js] - " + msg);
  }
}
