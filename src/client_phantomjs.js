// module imports
var page = require('webpage').create();
var system = require('system');
var fs = require('fs');

// program configurarions variables
var WRITE_TO_FILE_THRESHOLD = 100;
var CSP_REGEX = new RegExp("content-security-policy|^x-.*-csp(-report-only)?","i");
var CSV_FILE = null;
var START_DOMAIN = 1; // index as in Alexa top 1Mio, i.e. starting with 1
var TOP_DOMAINS = 10; // index as in Alexa top 1Mio, i.e. starting with 1
var PAGE_LOADING_TIME = 5000; // in ms
var PAGE_LOAD_TIMEOUT = PAGE_LOADING_TIME+1000; // in ms
var CLOSING_COUNTDOWN = 5000; // in ms
var CSP_VIOLATION_STR =
      "because it violates the following Content Security Policy directive";

var domains = [];
var currentDomainIndex = 0;


/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/


main(system.args);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameter(args);
  readDomainsFromFile(CSV_FILE, function() {
    log("I", "#domains: "+domains.length);
    currentDomainIndex = 0;
    visitNextDomain(currentDomainIndex);
  });
}

function initPage() {
  if (page) { page.close(); }
  page = require('webpage').create();
  page.settings.userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"
  page.onError = function(msg, trace) {
    log("E", "ERROR: " + msg);
    log("D", "TRACE: "+JSON.stringify(trace));
  };
  page.viewportSize = { width: 1920, height: 1080 };
  injectDOMModifiedListener();
}

function injectDOMModifiedListener() {
  page.onConsoleMessage = function(msg, lineNum, sourceId) {
    log('CONSOLE: ', msg);
  };

  page.onInitialized = function() {
    var success = page.evaluate(function() {
      var config = { childList: true, subtree: true };
      var mo = new MutationObserver(function(mutations, that) {
        if (typeof window.callPhantom === 'function') {
          var nodes = [];
          mutations.forEach(function(mutation) {
            for (var i = 0; i < mutation.addedNodes.length; i++) {
              var item = mutation.addedNodes.item(i);
              if ((item && item.tagName && item.baseURI && item.attributes)
                  && (item.tagName.toUpperCase() === "META")) {
                nodes.push({
                  baseURI: item.baseURI,
                  attributes: item.attributes
                });
              }
            };
          }); // forEach end
          window.callPhantom(nodes);
        }
        that.takeRecords(); // flush it
      }); // mutation observer function end
      mo.observe(document, config);
      /*
      var old = document.createElement;
      document.createElement = function(str) {
        console.log("Element created: "+str);
        return old(this, str);
      }
      //*/
      return true;
    }); // injection script end

    if (success) {
      log("I", 'DOM listener injected into page sucessfully.');
    } else {
      log("E", 'Failed to inject DOM listener into page.');
    }
  };
}

function visitNextDomain(domain_index) {
  setTimeout(function() {
    if (0 <= domain_index && domain_index < domains.length) {
      if ((0 < domain_index) && ((domain_index % WRITE_TO_FILE_THRESHOLD) === 0)) {
        var startStr = domains[0].index;
        var endStr = domains[domain_index-1].index;
        var outputArray = domains.splice(0,WRITE_TO_FILE_THRESHOLD);
        writeToFile(outputArray, startStr, endStr, function() {
          currentDomainIndex = 0;
          visitNextDomain(currentDomainIndex);
        });
      } else {
        initPage(); // reset the page, i.e. close and create a new instance
        domains[domain_index].visitWebpages();
      }
    } else {
      log("\nI", "DONE :-)\n");
      setTimeout(function() {
        log("I", "Waiting for last pending requests/responses... ("+PAGE_LOAD_TIMEOUT+"s)");
        var startStr = domains[0].index;
        var endStr = domains[domains.length-1].index;
        writeToFile(domains, startStr, endStr, function() {
          shutdown();
        });
      }, PAGE_LOAD_TIMEOUT);
    }
  }, 0);
}

//*
function Domain(index, domainName) {
  this.index = index;
  this.domain = domainName;
  this.urlsToTest = [
    { url: "http://"+domainName, policy: [], warnings: [] },
    { url: "http://www."+domainName, policy: [], warnings: [] },
    { url: "https://"+domainName, policy: [], warnings: [] },
    { url: "https://www."+domainName, policy: [], warnings: [] }
  ];
  this.currentURL = -1;

  this.visitWebpages = function() {
    var that = this;

    // page.onResourceRequested = function(request) { };

    page.onCallback = function(data) {
      for (var i = 0; i < data.length; i++) {
        if (data[i] && data[i].baseURI && data[i].attributes) {
          var header_name = null;
          var value = null;
          for (var a = 0; a < data[i].attributes.length; a++) {
            var attr = data[i].attributes[a];
            if (attr.name.match(new RegExp("http-equiv","i"))
                && attr.value.match(CSP_REGEX)) {
              header_name = attr.value;
            } else
            if (attr.name.match(new RegExp("content","i"))) {
              value = attr.value;
            }
          } // for attributes.length
          if (header_name && value) {
            that.urlsToTest[that.currentURL].policy.push({
              url: data[i].baseURI,
              meta: { name: header_name, value: value }
            });
            log("I", "META http-equiv='"+value+"'");
          }
        }
      }
    };

    page.onResourceError = function(resourceError) {
        that.onLoadFailed(resourceError);
    };

    page.onResourceReceived = function(response) {
      if (response && response.headers) {
        //console.log('Response ' + response.url);
        response.headers.forEach(function(header) {
          if (header.name.match(CSP_REGEX)) {
            var responseURL = (response.redirectURL) ? response.redirectURL : response.url;
            //console.log("CSP found! url: "+responseURL);
            that.urlsToTest[that.currentURL].policy.push({
              url: responseURL,
              http: { name: header.name, value: header.value }
            });
          }
        });
      }
    };

    if (this.currentURL < this.urlsToTest.length-1) {
      this.currentURL++;
      log("I", "Try to load '"+this.urlsToTest[this.currentURL].url+"' (index: "
                +this.index+")...", true);
      page.open(
        this.urlsToTest[this.currentURL].url,
        function(status) {
          if (status === "success") {
            that.onLoadSuccess();
          }
          // else case this is handled by onResourceError function
        }
      );
      // wait on the current page to allow dynamic subresources to be loaded
      // do it here to guarantee a max page loading time (incl. page loading
      // itself)
      window.setTimeout(this.timeoutFunction, PAGE_LOAD_TIMEOUT);

    } else {
      log("I","Domain processing finished ("+this.domain+")");
      log("I","-------------------");
      currentDomainIndex = currentDomainIndex + 1;
      visitNextDomain(currentDomainIndex);
    }
  };

  this.onLoadSuccess = function () {
    log("I", "Page loaded. Waiting for subresources...");
    //log("D", page.frameContent);
  };

  this.onLoadFailed = function(reason) {
    log("I", "Page load failed: "+JSON.stringify(resourceError));
    this.urlsToTest[that.currentURL].warnings.push(reason);
    clearTimeout(this.timeoutFunction);
    this.timeoutFunction();
  };

  this.timeoutFunction = function() {
    //page.render('webpage.jpeg', {format: 'jpeg', quality: '100'});
    visitNextDomain(currentDomainIndex);
  };
}

function writeToFile(targetArray, startStr, endStr, callback) {
  var file_name = (new Date()).toISOString().slice(0,10).replace(/-/g,"")
                    +"_"+startStr+"_"+endStr+".txt";
  log("I", "Writing logs to file '"+file_name+"'...");
  try {
    var stream = fs.open("../logs/"+file_name, 'w');
    stream.write(JSON.stringify(targetArray, null, 2));
    stream.close();
  } catch (e) {
    log("E", "Failed to write to file: "+e.message);
  } finally {
    setTimeout(callback, 0);
  }
}

function init_parameter(args) {
  // args[0] = script
  for (var i = 1; i < args.length; i++) {
    if (args[i] == "-example") {
      domains.push(new Domain(domains.length+1, "example.com"));
    } else
    if (args[i] == "-csv" && i+1 < args.length) {
      CSV_FILE = ""+args[i+1];
      i++;
    } else
    if (args[i] == "-url" && i+1 < args.length) {
      domains.push(new Domain(domains.length+1, args[i+1]));
      i++;
    } else
    if (args[i] == "-start" && i+1 < args.length) {
      START_DOMAIN = parseInt(args[i+1]);
      i++;
    } else
    if (args[i] == "-top" && i+1 < args.length) {
      TOP_DOMAINS = parseInt(args[i+1]);
      i++;
    } else
    if (args[i] == "-plt" && i+1 < args.length) {
      PAGE_LOADING_TIME = parseInt(args[i+1])*1000;
      PAGE_LOAD_TIMEOUT = PAGE_LOADING_TIME+1000;
      i++;
    }
    else {
      log("E", help_str());
      phantom.exit(1);
    }
  }
}

function readDomainsFromFile(csv_file, callback) {
  log("I", "calling readDomainsFromFile");
  if (isString(csv_file) && domains && domains.length == 0) {
    log("I", "Reading domains from CSV...");
    try {
      var stream = fs.open(csv_file, 'r');
      var line = null;
      var finishReading = false;
      while (!finishReading && (line = stream.readLine())) {
        var data = line.split(",");
        if (START_DOMAIN <= data[0] && data[0] <= TOP_DOMAINS) {
          //log("D", 'Adding ' + data + '...');
          domains.push(new Domain(data[0],data[1]));
          // stop reading when we read the last domain for our instance
          if (TOP_DOMAINS <= data[0]) { finishReading = true; }
        }
      }
      stream.close();
      log("I", "Initialisation complete");
    } catch (e) {
      log("E", "Failed to read from CSV: "+e.message);
      phantom.exit(1);
    }
    setTimeout(callback, 0);
  } else {
    log("I", "Manual URL list detected. Ignoring CSV file.");
    setTimeout(callback, 0);
  }
}
function isString(x) { return (typeof x === 'string' || x instanceof String); }

function shutdown() {
  log("I", 'Browser is shutting down now.');
  phantom.exit(0);
}

function log(mode, msg, update) {
  console.log(mode + " [client.js] - " + msg);
}

function help_str() {
  return "TODO";
}
