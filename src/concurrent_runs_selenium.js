'use strict'

// module imports
const exec = require('child_process').execFile;

// program configurarions variables
var PAUSE = 30*1000;
var NUM_DOMAINS = 1;
var TOTAL_TIME_IN_MIN = 60;
var SEC_PER_PAGE_LOAD = 30;


/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/

main(process.argv);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameter(args);
  process.env.PATH = process.env.PATH+":/home/cookie/research/csp_crawler/selenium/";

  // times 4 because of http://, https://, http://www. and https://www.
  var num_buckets = Math.ceil( (NUM_DOMAINS*4) / (TOTAL_TIME_IN_MIN * (60/SEC_PER_PAGE_LOAD)) );
  var bucket_size = Math.ceil( NUM_DOMAINS / num_buckets );

  log("I", "Creating "+num_buckets+" buckets of size "+bucket_size);

  var start = 1;
  var end = 1;

  startXvfb();
  setTimeout( function() {
    if (start <= NUM_DOMAINS) { spawnBrowser(); }
  }, 3000 );


  function spawnBrowser() {
    end = start + bucket_size - 1;
    if (NUM_DOMAINS < end) {
      end = NUM_DOMAINS;
    }

    log("I", "Start: "+start+", end: "+end);
    startCrawling(start, end, SEC_PER_PAGE_LOAD);

    start = end + 1;
    if (start <= NUM_DOMAINS) { setTimeout(spawnBrowser, PAUSE); }
  }

}


function startXvfb() {
  var bash_cmd = "Xvfb :10 -ac";
  var terminal_cmd =
    'gnome-terminal --window-with-profile=Experiments -e "bash -c \\\"'+bash_cmd+'\\\""';

  var options = {
    cwd: "/home/cookie/research/csp_crawler/src/",
    shell: "bash",
    uid: 1000,
    gid: 1000
  };

  exec(terminal_cmd, options, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    if (stdout) {
      console.log(`stdout: ${stdout}`);
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
    }
  });
}


function startCrawling(start, end, page_loading_in_sec) {
  var bash_cmd = "nodejs client.js -csv ../top-1m.csv -ct 5 "
                  +"-start "+start+" -top "+end+" -plt "+page_loading_in_sec;
  bash_cmd = "export DISPLAY=:10; echo \\\"DISPLAY=\\\$DISPLAY\\\"; "+ bash_cmd;
  var terminal_cmd =
    'gnome-terminal --window-with-profile=Experiments -e "bash -c \\\"'+bash_cmd+'\\\""';

  var options = {
    cwd: "/home/cookie/research/csp_crawler/src/",
    shell: "bash",
    uid: 1000,
    gid: 1000
  };

  exec(terminal_cmd, options, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    if (stdout) {
      console.log(`stdout: ${stdout}`);
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
    }
  });
}


function init_parameter(args) {
  for (var i = 0; i < args.length; i++) {
    if (2 <= i) { // "nodejs" and the script file are 0 and 1, respectively
      if ((args[i] == "-domains" || args[i] == "-d") && i+1 < args.length) {
        NUM_DOMAINS = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "-totaltime" || args[i] == "-tt") && i+1 < args.length) {
        TOTAL_TIME_IN_MIN = parseInt(args[i+1]);
        i++;
      } else
      if ((args[i] == "-secperload" || args[i] == "-spl") && i+1 < args.length) {
        SEC_PER_PAGE_LOAD = parseInt(args[i+1]);
        i++;
      }
      else {
        log("E", help_str());
        process.exit();
      }
    }
  }
}


function help_str() {
  return "Usage: TODO";
}


function log(mode, msg, update=false) {
  if (update) {
    process.stdout.write(mode + " [client.js] - " + msg+'\r');
  } else {
    console.log(mode + " [client.js] - " + msg);
  }
}
