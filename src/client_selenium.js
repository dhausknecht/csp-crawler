'use strict'

// module imports
var webdriver = require('../selenium/node_modules/selenium-webdriver');
var proxy = require('../selenium/node_modules/selenium-webdriver/proxy');
var logging = require('../selenium/node_modules/selenium-webdriver').logging;
var By = require('../selenium/node_modules/selenium-webdriver').By;
var Key = require('../selenium/node_modules/selenium-webdriver').Key;
var fast_csv = require('fast-csv');
var fs = require('fs');

// program configurarions variables
var CSV_FILE = null;
var START_DOMAIN = 1; // index as in Alexa top 1Mio, i.e. starting with 1
var TOP_DOMAINS = 10; // index as in Alexa top 1Mio, i.e. starting with 1
var PAGE_LOADING_TIME = 5000; // in ms
var PAGE_LOAD_TIMEOUT = PAGE_LOADING_TIME+1000; // in ms
var CLOSING_COUNTDOWN = 5000; // in ms
var LOG_LEVEL = logging.Level.ALL;
var EXTENSION_URL =
      "chrome-extension://mcdgkdhajbonbjandncjcdeakfamlbhj/background.html";
var CSP_VIOLATION_STR =
      "because it violates the following Content Security Policy directive";

var domains = [];
var currentDomainIndex = 0;
var browser = null;


/******************************************************************************
 * Program                                                                    *
 ******************************************************************************/

main(process.argv);


/******************************************************************************
 * Functions                                                                  *
 ******************************************************************************/

function main(args) {
  init_parameter(args);
  browser = getWebDriver();
  browser.get(EXTENSION_URL)
  .then(function() {
    var waitEnd = new Date().getTime() + 2000;
    return browser.wait(function() { return (waitEnd <= new Date().getTime()); }, 3000);
  })
  .then(function() { return browser.findElement(By.css('body')); })
  .then(function(elem) { return elem.sendKeys(Key.CONTROL, "t"); })
  .then(function() { return browser.getAllWindowHandles(); })
  .then(function(handles) { return browser.switchTo().window(handles[handles.length-1]); })
  .then(function() {
    readDomainsFromFile(CSV_FILE, function() {
      log("I", "#domains: "+domains.length);
      currentDomainIndex = 0;
      visitNextDomain(currentDomainIndex);
    });
  });
}

function visitNextDomain(domain_index) {
  if (0 <= domain_index && domain_index < domains.length) {
    domains[domain_index].visitWebpages();
  } else {
    log("\nI", "DONE :-)\n");
    finalise().then(function() {
      countDownClosingTime(CLOSING_COUNTDOWN);
    });
  }
}

function Domain(index, domainName) {
  this.index = index;
  this.domain = domainName;
  this.urlsToTest = [
    { url: "http://"+domainName, policy: null, warnings: [] },
    { url: "http://www."+domainName, policy: null, warnings: [] },
    { url: "https://"+domainName, policy: null, warnings: [] },
    { url: "https://www."+domainName, policy: null, warnings: [] }
  ];
  this.currentURL = -1;

  this.visitWebpages = function() {
    if (this.currentURL < this.urlsToTest.length-1) {
      this.currentURL++;
      log("I", "Try to load '"+this.urlsToTest[this.currentURL].url+"' (index: "
                +this.index+")...", true);
      var that = this;
      browser
        .get(this.urlsToTest[this.currentURL].url)
        .then(function() { that.onLoadSuccess(); })
        .thenCatch(function(reason) { that.onLoadFailed() });

    } else {
      log("I","Domain processing finished ("+this.domain+")");
      log("I","-------------------");
      currentDomainIndex = currentDomainIndex + 1;
      visitNextDomain(currentDomainIndex);
    }
  };

  this.onLoadSuccess = function () {
    log("I", "Try to load '"+this.urlsToTest[this.currentURL].url+"'... done.");
    log("I", "Try to load '"+this.urlsToTest[this.currentURL].url+"' "
              +"(index: "+this.index+"...");
    var that = this;

    // wait on the current page to allow dynamic subresources to be loaded
    var startTime = new Date().getTime();
    var outputTime = startTime;
    var endTime = startTime + PAGE_LOADING_TIME;
    browser.wait(function() {
        var now = (new Date().getTime())-1000;
        if (endTime <= now) { return true; }
        else if (outputTime <= now) {
          if ((endTime-outputTime) <= 1000) {
            log("I", 'Waiting for webpage... 1s', true);
          } else {
            log("I", 'Waiting for webpage... '+((endTime-outputTime)/1000)+"s", true);
          }
          outputTime = outputTime + 1000;
        } else {
          return false;
        }
      },PAGE_LOAD_TIMEOUT)
    .then(function() {
      log("I", 'Waiting for webpage... done');
      return browser.getAllWindowHandles();
    })
    // get the interesting HTTP headers from the extension
    .then(function(handles) { return browser.switchTo().window(handles[0]); })
    .then(function() {
      log("I", "Requesting policy from extension...", true);
      return browser.executeScript("return JSON.stringify(getHeaderInfo())");
    })
    .then(function(res) {
      log("I", "Requesting policy from extension... done");
      that.urlsToTest[that.currentURL].policy = JSON.parse(res);
      return that.getPageLogs("extension");
    })
    // switch back to the browsing tab
    .then(function() { return browser.getAllWindowHandles(); })
    .then(function(handles) { return browser.switchTo().window(handles[handles.length-1]); })
    .then(function() { return that.getPageLogs("page"); })
    .then(function() { visitNextDomain(currentDomainIndex); })
    .catch(function(reason) {
      log("E", "catch: "+reason);
      that.urlsToTest[that.currentURL].warnings.push("[ERROR] - "+reason);
      visitNextDomain(currentDomainIndex);
    });
  };

  this.onLoadFailed = function(reason) {
    log("I", "Page load failed: "+reason);
    var that = this;
    // make sure we are in the browsing tab
    browser.getAllWindowHandles()
    .then(function(handles) { return browser.switchTo().window(handles[handles.length-1]); })
    .then(function() { return that.getPageLogs("page"); })
    .then(function() { visitNextDomain(currentDomainIndex); });
  };

  this.getPageLogs = function(target) {
    var that = this;
    log("I", "getting "+target+" logs...");
    return new Promise(function(resolve, reject) {
      browser.manage().logs().get(logging.Type.BROWSER).then(function(entries) {
        if (entries) {
          entries.forEach(function(msg) {
            log("I", "["+msg.level+"] "+msg.message);
            if (msg.message.indexOf(CSP_VIOLATION_STR) !== -1) {
              that.urlsToTest[that.currentURL].warnings.push(msg.message);
            }
          });
        }
        resolve();
      });
    });
  };
}

function finalise() {
  return new Promise(function(resolve, reject) {
    var file_name = (new Date()).toISOString().slice(0,10).replace(/-/g,"")
                      +"_"+START_DOMAIN+"_"+TOP_DOMAINS
                      +".txt";
    var wstream = fs.createWriteStream("../logs/"+file_name);
    wstream.write(JSON.stringify(domains, null, 2));
    wstream.end();
    resolve();
  });
}


function init_parameter(args) {
  for (var i = 0; i < args.length; i++) {
    if (2 <= i) { // "nodejs" and the script file are 0 and 1, respectively
      if (args[i] == "-example") {
        domains.push(new Domain(domains.length+1, "example.com"));
      } else
      if (args[i] == "-csv" && i+1 < args.length) {
        CSV_FILE = ""+args[i+1];
        i++;
      } else
      if (args[i] == "-url" && i+1 < args.length) {
        domains.push(new Domain(domains.length+1, args[i+1]));
        i++;
      } else
      if (args[i] == "-start" && i+1 < args.length) {
        START_DOMAIN = parseInt(args[i+1]);
        i++;
      } else
      if (args[i] == "-top" && i+1 < args.length) {
        TOP_DOMAINS = parseInt(args[i+1]);
        i++;
      } else
      if (args[i] == "-ct" && i+1 < args.length) {
        CLOSING_COUNTDOWN = parseInt(args[i+1])*1000;
        i++;
      } else
      if (args[i] == "-plt" && i+1 < args.length) {
        PAGE_LOADING_TIME = parseInt(args[i+1])*1000;
        PAGE_LOAD_TIMEOUT = PAGE_LOADING_TIME+1000;
        i++;
      }
      else {
        log("E", help_str());
        process.exit();
      }
    }
  }
}

function readDomainsFromFile(csv_file, callback) {
  if (isString(csv_file) && domains && domains.length == 0) {
    log("I", "Reading domains from CSV...");
    var stream = fs.createReadStream(csv_file);
    fast_csv
      .fromStream(stream)
      .on("data", function(data) {
        if (START_DOMAIN <= data[0] && data[0] <= TOP_DOMAINS) {
          //log("D", 'Adding ' + data + '...');
          domains.push(new Domain(data[0],data[1]));
        }
      })
      .on("end", function() {
        log("I", "Initialisation complete");
        callback();
      });
  } else {
    log("I", "Manual URL list detected. Ignoring CSV file.");
    callback();
  }
}
function isString(x) { return (typeof x === 'string' || x instanceof String); }

function getWebDriver() {
  var prefs = new logging.Preferences();
  prefs.setLevel(logging.Type.BROWSER, LOG_LEVEL);

  /*
  http://stackoverflow.com/questions/27278222/is-it-possible-to-add-a-plugin-to-chromedriver-under-a-protractor-test/27278322#27278322
  to avoid base64 encoding problems and stuff... *sigh*
  */
  var caps = webdriver.Capabilities.chrome().set('chromeOptions', {
    args: ['--load-extension=/home/cookie/research/csp_crawler/chrome_extension/']
  });
  var driver = new webdriver.Builder()
              .withCapabilities(caps)
              .setLoggingPrefs(prefs)
              .build();
  driver.manage().window().maximize();
  driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT);
  return driver;
};

function countDownClosingTime(closing_time) {
  if (closing_time == undefined) return;
  log("I", 'Browser will close in ' + (closing_time/1000) + 's...',true);
  if (closing_time <= 1000) {
    browser.sleep(closing_time).then(function() {
      log("I", 'Browser will close now...', true);
      var closeTabs = function() {
        browser.getAllWindowHandles()
        .then(function(handles) {
          browser.switchTo().window(handles[handles.length-1])
          .then(function() {
            browser.close();
            if (1 < handles.length) {
              closeTabs();
            } else {
              log("I", 'Browser will close now...  done');
            }
          });
        });
      };
      closeTabs();
    });
  } else {
    browser.sleep(1000).then(function() {
      countDownClosingTime(closing_time - 1000);
    });
  }
}

function help_str() {
  return "Usage: nodejs path/to/js_file.js -csv <path/to/csv> "
        +"[ -ct <countdown in sec> | -url <custom URL> | -top <# of domains> ]\n"
        +"\tNote: do NOT use brackets for file paths!"
}

function log(mode, msg, update=false) {
  if (update) {
    process.stdout.write(mode + " [client.js] - " + msg+'\r');
  } else {
    console.log(mode + " [client.js] - " + msg);
  }
}
